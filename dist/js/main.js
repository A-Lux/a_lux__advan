$( document ).ready(function(){
  $('.slick-slider').slick({
    dots: true,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 2000,
    speed: 500, 
    fade: true,
    cssEase: 'linear'
  });
});